System.register(['@angular/http', '@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1;
    var BookmarkService;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            BookmarkService = (function () {
                function BookmarkService(http) {
                    this.http = http;
                    this.errorHandler = function (error) { return console.error('BookmarkService error', error); };
                    this.baseUrl = 'https://ang2-678b9.firebaseio.com/';
                }
                BookmarkService.prototype.updateBookmark = function (bookmark) {
                    var json = JSON.stringify({
                        Title: bookmark.Title,
                        Url: bookmark.Url
                    });
                    return this.http.patch(this.baseUrl + "bookmarks/" + bookmark.id + ".json", json)
                        .toPromise()
                        .catch(this.errorHandler);
                };
                BookmarkService.prototype.addBookmark = function (bookmark) {
                    var json = JSON.stringify(bookmark);
                    return this.http.post(this.baseUrl + "bookmarks.json", json)
                        .toPromise()
                        .catch(this.errorHandler);
                };
                BookmarkService.prototype.removeBookmark = function (bookmark) {
                    return this.http.delete(this.baseUrl + "bookmarks/" + bookmark.id + ".json")
                        .toPromise()
                        .catch(this.errorHandler);
                };
                BookmarkService.prototype.getBookmarks = function () {
                    var _this = this;
                    return this.http.get(this.baseUrl + "bookmarks.json")
                        .toPromise()
                        .then(function (response) { return _this.convert(response.json()); })
                        .catch(this.errorHandler);
                };
                BookmarkService.prototype.convert = function (convertResponse) {
                    return Object.keys(convertResponse)
                        .map(function (id) { return ({
                        id: id,
                        Title: convertResponse[id].Title,
                        Url: convertResponse[id].Url
                    }); });
                };
                BookmarkService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], BookmarkService);
                return BookmarkService;
            }());
            exports_1("BookmarkService", BookmarkService);
        }
    }
});
